# River Sampling

Computes the spatial distribution of measurement station on a river under a maximum distance objective.

The script considers an _a priori_ set of measures points and that measures will be done at each confluence.
Then it will distribute new measure points on section without measure that override a maximum length.

Edit the file ```river-sampling.py``` for adjusting the input parameters:
 - ```river_inputfilename```: path to the input file that describes the hydrographic network. Supported formats: at least ```GeoJSON``` and ```ESRI Shapefile``` (through fiona library).
 - ```measures_inputfilename```: path to the input file that describes the _a priori_ measures points. Supported formats: at least ```GeoJSON``` and ```ESRI Shapefile``` (through fiona library).
 - ```output_filename```: path to the resulting output file that will contain the new measure points computed.
 - ```max_distance```: the maximum distance between two measures points.

Usage:
``` sh
python3 river-sampling.py
```

You can also look at the notebook file that displays intermediary results.
