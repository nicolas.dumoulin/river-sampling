import json
import fiona
from shapely.ops import split
from shapely.geometry import Point, LineString, shape
import networkx as nx
from math import radians, cos, sin, asin, sqrt, atan2,ceil

river_inputfilename="river_4326.geojson"
river_inputfilename="dataCapucine/Test_Basin.geojson"
#river_inputfilename="river.shp"
measures_inputfilename="measures_4326.geojson"
measures_inputfilename="dataCapucine/map.geojson"
output_filename="newMeasures.shp"
max_distance=2000 # in meters

with open(river_inputfilename) as f:
    features = json.load(f)["features"]
lines = []
for feat in features:
    s = shape(feat["geometry"])
    if s.geom_type == 'LineString':
        lines.append(s)
    elif s.geom_type == 'MultiLineString':
        for l in s.geoms:
            if l.geom_type != 'LineString':
                print('Erreur, cette primitive devrait être une ligne : '+str(l))
            lines.append(l)
    else:
        print('Erreur, ce type de primitives n\'est pas supporté : '+str(s.geom_type))
measures = [Point(point['geometry']['coordinates']) for point in fiona.open(measures_inputfilename)]

# Find confluences
G = nx.Graph()
for line in lines:
   # convert the line segments to Graph edges
   for seg_start, seg_end in zip(line.coords,line.coords[1:]):
       G.add_edge(seg_start, seg_end)
confluences = [Point(node) for node in G.nodes if G.degree(node) > 2]
for node in G.nodes:
    if G.degree(node) > 2:
        confluences.append(Point(node))
# Snap measure points on river
projMeasures=[]
for m in measures:
    # found the nearest line to the measure
    dmin=lines[0].distance(m)
    idx_dmin=0
    for i,l in enumerate(lines[1:]):
        d=l.distance(m)
        if d<dmin:
            idx_dmin=i+1
            dmin=d
    # projetting the point on the line
    projP = lines[idx_dmin].interpolate(lines[idx_dmin].project(m))
    projMeasures.append(projP)
    # inserting the projetted point in the line
    for i,(a,b) in enumerate(zip(lines[idx_dmin].coords,lines[idx_dmin].coords[1:])):
        if LineString([a,b]).distance(projP)<1e-4:
            newLine=LineString(lines[idx_dmin].coords[:i+1] + [projP] + lines[idx_dmin].coords[i+1:])
            break
    lines[idx_dmin] = newLine
# Split at measures andconfluences
splitPts = confluences + projMeasures
waterways = lines[:]
newwaterways=[]
for waterway in waterways:
    ways=[waterway]
    for p in splitPts:
        nextways=[]
        for way in ways:
            if way.contains(p):
                splitted = list(split(way,p))
                nextways.extend(splitted)
            else:
                nextways.append(way)
        ways=nextways
    newwaterways.extend(ways)
def haversine(coords):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    """
    ((lon1, lat1), (lon2, lat2)) = coords
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * atan2(sqrt(a), sqrt(1-a))
    return 6367000 * c
# Computing position of new measures
newMeasurePoints = []
for w in newwaterways:
    distW = 0
    for coords in zip(w.coords,w.coords[1:]):
        distW += haversine(coords)
    if distW > max_distance:
        # how many division we want
        nbDiv = ceil(distW / max_distance)
        # the target length of each division
        lenDiv = distW / nbDiv
        cumulDist = 0
        nbNewPts = 0
        for a,b in zip(w.coords,w.coords[1:]):
            dist = haversine((a,b))
            while cumulDist+dist > lenDiv:
                ratio = (lenDiv - cumulDist) / dist
                newX = a[0] + (b[0] - a[0])*ratio
                newY = a[1] + (b[1] - a[1])*ratio
                newMeasurePoints.append([newX,newY])
                nbNewPts += 1
                if nbNewPts == nbDiv-1:
                    break
                cumulDist = 0
                dist -= haversine((a,(newX,newY)))
                a=(newX,newY)
            if nbNewPts == nbDiv-1:
                break
            else:
                cumulDist += dist
schema = {'geometry': 'Point','properties':{}}
crs = {'init': 'epsg:4326'}
shp = fiona.open(output_filename,mode='w',schema=schema,driver='ESRI Shapefile',crs=crs)
for p in newMeasurePoints:
    shp.write({ 'geometry':{'type':'Point','coordinates':(p[0],p[1])},'properties':{} })
shp.close()
